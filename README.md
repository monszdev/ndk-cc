# ndk-cc

The convenient way to prepare Android cross compilation.

As you may known, there are too many open source libraries in the world. Some of that libraries do not have Android distribution nor you have to build it from source for your Android project. But setting up a cross-compile toolchain from Android NDK is too complex. You have to know the differences between Android NDK revision and you also have to know how toolchain is working.

If you are looking for the convenient way to do that works, here you are.

## Acknowledge

Not all of project can use this tool.  
It depends on which style, how complex of makefiles, what dependencies of your project is using.  
Of course this script does not work on Windows.

## Prerequisites

* Android NDK is installed (r16 or higher).
* Project must be autotools or CMake style. Other style will not working.
* automake and cmake must also be installed as well.

## Usage

Prepare your environment

```sh
export ANDROID_NDK=<NDK path>
source ndk-cc.sh
```

Take the rest by only one command

```sh
ndk-cc [-a <architecture>] [-i <api level>] [-s <project style>] [-d <project dir>] [-c]
```

Not all of parameters need to be defined. These already have a default value
* `-a` specify target architecture. Default is `arm64`. Available value is `arm`, `arm64`, `x86`, `x86_64`
* `-i` specify target platform API level. Default is `21`
* `-s` specify your project style. Default is auto detect (CMake is preferred)
* `-d` specify your source path. Default is current working directory
* `-c` if you want to run the generator command immediately (configure or cmake)

## Example

libpcap is used to do an example for both CMake and autotools style project

### CMake

```sh
git clone https://github.com/the-tcpdump-group/libpcap
cd libpcap
mkdir build && cd build
ndk-cc -c -d ..
make
```

### autotools

```sh
git clone https://github.com/the-tcpdump-group/libpcap
cd libpcap
ndk-cc -c -s autotools
make
```

## Explaination

> COMING SOON !!!

## Author

Script by Duong Hong Hung (monszdev@gmail.com).  
Feel free if you want to fork, copy or contribute to this project.
