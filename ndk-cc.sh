
function getAbsPath() {
    if [ -d "$1" ]; then
        # dir
        (cd "$1"; pwd)
    elif [ -f "$1" ]; then
        # file
        if [[ $1 = /* ]]; then
            echo "$1"
        elif [[ $1 == */* ]]; then
            echo "$(cd "${1%/*}"; pwd)/${1##*/}"
        else
            echo "$(pwd)/$1"
        fi
    fi
}

function getNdkVersion() {
  
  if [[ -z "$ANDROID_NDK" ]]; then
    echo "ANDROID_NDK variable not initialized yet !!!"
    return 1
  fi
  
  if [[ ! -f "$ANDROID_NDK/source.properties" || ! -f "$ANDROID_NDK/ndk-build" ]]; then
    echo "ANDROID_NDK initialized but not correctly"
    return 1
  fi
  local ndkVersion=$(cat $ANDROID_NDK/source.properties | grep Pkg.Revision | sed -r 's/[A-Za-z= .]+//g' | awk '{print substr($0, 1, 2)}')
  echo $ndkVersion
  return 0
}

function getHostConfig() {
  
  local host_name=$(uname -s | awk '{print tolower($0)}')
  local host_arch=$(uname -m)
  
  if [[ "$host_arch" != "x86_64" ]]; then
    echo "Unsupported architecture $host_arch"
    return 1
  fi
  local prebuiltName="$host_name-$host_arch"
  echo $prebuiltName
  return 0
}

function getTargetHost() {
  
  local targetArch=$1
  local targetHost=$targetArch
  
  case $targetArch in
    arm)   targetHost=arm-linux-androideabi;;
    arm64) targetHost=aarch64-linux-android;;
  esac
  echo $targetHost
  return 0
}

function getAndroidAbi() {
  
  local targetArch=$1
  local androidAbi=$targetArch
  
  case $targetArch in
    arm)   androidAbi=armeabi-v7a;;
    arm64) androidAbi=arm64-v8a;;
  esac
  echo $androidAbi
}

## NDK r19 or above (r19-r22)
function prepareInplaceToolchain() {
  
  local ndkVersion=$1
  local targetArch=$2
  local compileApi=$3
  local runCommand=$4
  local targetHost=$(getTargetHost $targetArch)
  
  hostConfig=$(getHostConfig)
  if [[ $? != 0 ]]; then
    echo "Prepare host config error => $hostConfig"
    return 1
  fi
  echo "+ hostConfig = $hostConfig"
  
  local installDir=${ANDROID_NDK}/toolchains/llvm/prebuilt/${hostConfig}
  local binUtilsPrefix=${targetHost}
  local compilerPrefix=${targetHost}
  if [[ $ndkVersion -gt 21 ]]; then
    binUtilsPrefix=llvm
  fi
  if [[ $targetArch == "arm" ]]; then
    compilerPrefix=armv7a-linux-androideabi
  fi
  export AS=${installDir}/bin/${binUtilsPrefix}-as
  export LD=${installDir}/bin/${binUtilsPrefix}-ld
  export CC=${installDir}/bin/${compilerPrefix}${compileApi}-clang
  if [[ $ndkVersion -gt 21 ]]; then
    export AS=${CC}
    export LD=${installDir}/bin/ld
  fi
  
  export AR=${installDir}/bin/${binUtilsPrefix}-ar
  export AS=${AS}
  export LD=${LD}
  export NM=${installDir}/bin/${binUtilsPrefix}-nm
  export RANLIB=${installDir}/bin/${binUtilsPrefix}-ranlib
  export STRIP=${installDir}/bin/${binUtilsPrefix}-strip
  export CC=${CC}
  export CXX=${CC}++
  if [[ "$runCommand" == "true" ]]; then
    ${projectDir}/configure --host=${compilerPrefix}
    return 0
  fi
  echo ""
  echo "******************************************************"
  echo "* Run this following command to prepare your project *"
  echo "******************************************************"
  echo ""
  echo "${projectDir}/configure --host=${compilerPrefix}"
  echo ""
}

## NDK r18 or below (r16-r18)
function prepareStandaloneToolchain() {
  
  local ndkVersion=$1
  local targetArch=$2
  local compileApi=$3
  local runCommand=$4
  
  local setInstall=true
  local targetHost=$(getTargetHost $targetArch)
  
  local installDir=$(dirname $ANDROID_NDK)/${targetHost}_r${ndkVersion}-api${compileApi}
  if [[ -d "$installDir" ]]; then
    echo "Standalone toolchain already installed => $installDir"
    echo -n "Force install again? [y/N] "
    read ans
    case $ans in
      [Yy]) setInstall=true;;
      *) setInstall=false;;
    esac
    echo "+ setInstall = $setInstall"
  fi
  if [[ "$setInstall" == true ]]; then
    $ANDROID_NDK/build/tools/make-standalone-toolchain.sh --arch=${targetArch} --platform=${compileApi} --install-dir=${installDir} --force
  fi
  
  export AR=${installDir}/bin/${targetHost}-ar
  export AS=${installDir}/bin/${targetHost}-as
  export LD=${installDir}/bin/${targetHost}-ld
  export NM=${installDir}/bin/${targetHost}-nm
  export RANLIB=${installDir}/bin/${targetHost}-ranlib
  export STRIP=${installDir}/bin/${targetHost}-strip
  export CC=${installDir}/bin/${targetHost}-gcc
  export CXX=${installDir}/bin/${targetHost}-g++

  if [[ "$runCommand" == "true" ]]; then
    ${projectDir}/configure --host=${targetHost}
    return 0
  fi
  echo ""
  echo "******************************************************"
  echo "* Run this following command to prepare your project *"
  echo "******************************************************"
  echo ""
  echo "${projectDir}/configure --host=${targetHost}"
  echo ""
}

function testOpt() {
  
  local targetArch=$1
  local compileApi=$2
  
  case $targetArch in
    arm|arm64)
      if [[ $compileApi -lt 21 ]]; then
        echo "Architecture $targetArch require API level 21 or higher. Currently is $compileApi"
        return 1
      fi
    ;;
    x86|x86_64)
    ;;
    *)
      echo "Unsupported architecture $targetArch"
      return 1
    ;;
  esac
  return 0
}

function prepare_autotools() {
  
  local targetArch=$1
  local compileApi=$2
  local projectDir=$3
  local runCommand=$4
  
  local ndkVersion=$(getNdkVersion)
  if [[ $? != 0 ]]; then
    echo "Prepare ANDROID_NDK error => $ndkVersion"
    return 1
  fi
  
  if [[ $ndkVersion -gt 18 ]]; then
    prepareInplaceToolchain $ndkVersion $targetArch $compileApi $runCommand
  else
    prepareStandaloneToolchain $ndkVersion $targetArch $compileApi $runCommand
  fi
}

function prepare_cmake() {
  
  local targetArch=$1
  local compileApi=$2
  local projectDir=$3
  local runCommand=$4
  
  local androidPlatform=android-${compileApi}
  local androidAbi=$(getAndroidAbi $targetArch)
  local ndkVersion=$(getNdkVersion)
  if [[ $? != 0 ]]; then
    echo "Prepare ANDROID_NDK error => $ndkVersion"
    return 1
  fi

  unset AR
  unset AS
  unset LD
  unset NM
  unset RANLIB
  unset STRIP
  unset CC
  unset CXX
  if [[ "$runCommand" == "true" ]]; then
    cmake \
      -DANDROID_ABI=${androidAbi} \
      -DANDROID_PLATFORM=${androidPlatform} \
      -DCMAKE_TOOLCHAIN_FILE=\${ANDROID_NDK}/build/cmake/android.toolchain.cmake \
      -S${projectDir}
    return 0
  fi
  echo ""
  echo "******************************************************"
  echo "* Run this following command to prepare your project *"
  echo "******************************************************"
  echo ""
  echo "cmake \\
  -DANDROID_ABI=${androidAbi} \\
  -DANDROID_PLATFORM=${androidPlatform} \\
  -DCMAKE_TOOLCHAIN_FILE=\${ANDROID_NDK}/build/cmake/android.toolchain.cmake \\
  -S${projectDir}"
  echo ""
}

function ndk-cc() {
  
  local targetArch=arm64
  local compileApi=21
  local projectVar=
  local projectDir=.
  local projectOut=.
  local runCommand=false

  while getopts "a:i:d:o:s:c" arg; do
    case "$arg" in
      a) targetArch=$OPTARG;;
      i) compileApi=$OPTARG;;
      # o) projectOut=$OPTARG;;
      s) projectVar=$OPTARG;;
      c) runCommand=true;;
      d) projectDir=$(getAbsPath $OPTARG);;
      *)
        echo "Usage: ndk-cc [-a <architecture>] [-i <api level>] [-s <project style>] [-d <project dir>] [-c]"
        return 1
        ;;
    esac
  done
  
  local testOptErr=$(testOpt $targetArch $compileApi)
  if [[ $? != 0 ]]; then
    echo "$testOptErr"
    return 1
  fi
  
  local ndkVersion=$(getNdkVersion)
  if [[ $? != 0 ]]; then
    echo "Prepare ANDROID_NDK error => $ndkVersion"
    return 1
  fi
  
  echo "Checked ANDROID_NDK => $ANDROID_NDK"
  echo "Checked environment"
  echo "+ ndkVersion = $ndkVersion"
  echo "+ targetArch = $targetArch"
  echo "+ compileApi = $compileApi"
  echo "+ projectDir = $projectDir"
  echo "+ runCommand = $runCommand"
  
  if [[ ! -z $projectVar ]]; then
    case "$projectVar" in
      cmake)
        if [[ ! -f ${projectDir}/CMakeLists.txt ]]; then
          echo "Could not detect CMakeLists.txt"
          return 1
        fi
        ;;
      autotools)
        if [[ ! -f ${projectDir}/configure || ! -f ${projectDir}/configure.ac ]]; then
          echo "Could not detect configure or configure.ac"
          return 1
        fi
        ;;
      *)
        echo "Unsupported project structure $projectVar"
        return 1
        ;;
    esac
  else
    if [[ -f ${projectDir}/configure || -f ${projectDir}/configure.ac ]]; then
      projectVar=autotools
    fi
    if [[ -f ${projectDir}/CMakeLists.txt ]]; then
      projectVar=cmake
    fi
  fi

  echo "Checked project dir => $projectVar"
  if [[ ! -z $projectVar ]]; then
    case "$projectVar" in
      cmake)     prepare_cmake $targetArch $compileApi $projectDir $runCommand;;
      autotools) prepare_autotools $targetArch $compileApi $projectDir $runCommand;;
      *)
        echo "Unsupported project structure $projectVar"
        return 1
        ;;
    esac
    return 0
  else
    echo "Could not found any compatible project structure"
    return 1
  fi
}
